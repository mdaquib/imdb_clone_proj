const express = require('express')
const router = express.Router()

// user route
const user = require('./user/routes')
router.use('/user', user)

// movie actor route
const movieAct = require('./moviesAndActors/routes')
router.use('/movieAct', movieAct)

module.exports = router