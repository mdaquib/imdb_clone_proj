const dbMovies = require('../../../models/movieActor/movies')
const dbLogin = require('../../../models/user/userLogin')
const dbActor = require('../../../models/movieActor/actors')


module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                success: false,
                msg: 'user not loggedin'
            })
        }else{
            try{
                if(req.decoded.userType == 'admin'){
                    if(!req.body.name || !req.files || !req.body.description || !req.body.genre || !req.body.releaseDate || !req.body.actorId ){
                        res.json({
                            success: false,
                            msg: 'please provide all details'
                        })
                    }else{
                        let movieData = await dbMovies.findOne({$and: [{name: req.body.name}, { releaseDate: req.body.releaseDate}]})
                        if(movieData || movieData != null){
                            res.json({
                                success: false,
                                msg: 'movie already exists'
                            })
                        }else{
                            let actorData = await dbActor.findOne({_id: req.body.actorId})
                            if(!actorData || actorData == null){
                                res.json({
                                    success: false,
                                    msg: 'actor not found'
                                })
                            }else{
                               await new dbMovies({
                                    name: req.body.name,
                                    photos: req.files['photo'][0].fieldname + '.' + req.files['photo'][0].mimetype,
                                    description: req.body.description,
                                    genre: req.body.genre,
                                    releaseDate: req.body.releaseDate,
                                    cast: {
                                        actorId: req.body.actorId,
                                        name: actorData.name
                                    }
                                }).save()
                                res.json({
                                    success: true,
                                    msg: 'movie updated'
                                })
                            }
                        }
                    }
                }else{
                    res.json({
                        success: false,
                        msg: "You are not allowed to add data"
                    })
                }
            } catch(err){
                res.json({
                    success: false,
                    msg: 'DB_ERROR'
                })
            }
        }
    }catch(err){
        res.json({
            success: false,
            msg: 'something went wrong'
        })
    }
}