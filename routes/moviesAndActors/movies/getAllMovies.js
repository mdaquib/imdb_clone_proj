const dbMovies = require('../../../models/movieActor/movies')


module.exports = async(req, res) => {
    try{
        let movieData = await dbMovies.find({})
        .sort('-addedOn')
        if(!movieData || movieData == null){
            res.json({
                success: false,
                msg: 'no movie found'
            })
        }else{
            let totalRatings = movieData[0].ratings
                .map(data => data.rating)
                .reduce((a,b) => a + b)
            let aggRatings = (totalRatings/movieData[0].ratings.length).toFixed(1)

            let commentData = movieData[0].comments
                .map(data => data.text)
                console.log(commentData)
            
            let mapObj = movieData.map(movie => {
                return {
                    name: movie.name,
                    photos: movie.photos,
                    description: movie.description,
                    genre: movie.genre,
                    cast: movie.cast,
                    releaseDate: movie.releaseDate, 
                }
            })

            res.json({
                success: true,
                movie: mapObj,
                ratings: aggRatings,
                comments: commentData
            })
        }
    } catch(err) {
        res.json({
            success: false,
            msg: 'err in fetching data',
            err: err
        })
    }
}