const dbactors = require('../../../models/movieActor/actors')
const dbMovies = require('../../../models/movieActor/movies')
const dbLogin = require('../../../models/user/userLogin')

module.exports = async(req, res) => {
    try{
        let emailData = await dbLogin.findOne({email: req.decoded.email})
        if(!emailData || emailData == null){
            res.json({
                success: false,
                msg: 'something went wrong or user is not loggedin'
            })
        }else{
            try{
                if(req.decoded.userType == 'admin'){
                    let movieData = await dbMovies.findById(req.params.movieId)
                    if(!movieData || movieData == null){
                        res.json({
                            success: false,
                            msg: 'movie not found'
                        })
                    }else{
                        if(!req.body.actorId){
                            res.json({
                                success: false,
                                msg: 'please enter actorId'
                            })
                        }else{
                            let actorData = await dbactors.findOne({_id: req.body.actorId})
                            if(!actorData || actorData == null){
                                res.json({
                                    success: false,
                                    msg: 'actorData not found'
                                })
                        }else{
                            if(!movieData.cast[0] || movieData.cast[0] == null){
                                let updateCast = await dbMovies.findOneAndUpdate({_id: req.params.movieId}, { $set: { cast: {actorId: req.body.actorId, name: actorData.name}} })
                                res.json({
                                    success: true,
                                    msg: 'cast updated'
                                })
                            }else{
                                if(movieData.cast[0].actorId == req.body.actorId){
                                    res.json({
                                        success: false,
                                        msg: 'actorData already exists in cast Array'
                                    })
                                }else{
                                    let updateCast = await dbMovies.findOneAndUpdate({_id: req.params.movieId}, { $set: { cast: {actorId: req.body.actorId, name: actorData.name}} })
                                    res.json({
                                        success: true,
                                        msg: 'cast updated'   
                                    })
                                }
                            }
                        }
                        } 
                    }
                }else{
                    res.json({
                        success: false,
                        msg: "You are not allowed to add data"
                    })
                }
            }catch(err){
                res.json({
                    success: false,
                    msg: 'err in saving data',
                    err: err
                })
            }
        }
    }catch(err){
        res.json({
            success: false,
            msg: 'somthing went wrong',
            err: err
        })
    }
}