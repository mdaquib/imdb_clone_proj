const dbLogin = require('../../../models/user/userLogin')
const dbMovies = require('../../../models/movieActor/movies')



module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                suuccess: false,
                msg: 'something went wrong or user not loggedin'
            })
        }else{
            try{
                if(!req.body.genre){
                    res.json({
                        success: false,
                        msg: 'please enter genre'
                    })
                }else{
                    let moviesArr = await dbMovies.find({})
                    let movieData =  moviesArr.filter(data => data.genre == req.body.genre)
                    console.log(movieData)
                    if(movieData.length !=0){
                        let totalRatings = movieData[0].ratings
                        .map(data => data.rating)
                        .reduce((a,b) => a + b)
                    let aggRatings = (totalRatings/movieData[0].ratings.length).toFixed(1)
                    let commentData = movieData[0].comments
                            .map(data => data.text)
                            console.log(commentData)
                        
                        let mapObj = movieData.map(movie => {
                            return {
                                name: movie.name,
                                photos: movie.photos,
                                description: movie.description,
                                genre: movie.genre,
                                cast: movie.cast,
                                releaseDate: movie.releaseDate, 
                            }
                        })
                        res.json({
                            success: true,
                            movie: mapObj,
                            ratings: aggRatings,
                            comments: commentData
                        })
                    }else{
                        res.json({
                            success: false,
                            msg: 'no movies found of this genre'
                        })
                    }
                }
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        res.json({
            success: false,
            msg: 'something went wrong',
            err: err
        })
    }
}