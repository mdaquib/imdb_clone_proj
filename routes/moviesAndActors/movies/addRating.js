const dbLogin = require('../../../models/user/userLogin')
const dbMovies = require('../../../models/movieActor/movies')




module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                suuccess: false,
                msg: 'something went wrong or user not loggedin'
            })
        }else{
            try{
                let movieData = await dbMovies.findById(req.params.movieId)
                if(!movieData || movieData == null){
                    res.json({
                        success: false,
                        msg: 'Movie not Found'
                    })
                }else{
                    if(!req.body.rating){
                        res.json({
                            success: false,
                            msg: 'Please add rating'
                        })
                    }else{
                        try{
                            const ratingObj = {
                                email: req.decoded.email,
                                name: loginData.name,
                                rating: req.body.rating
                            }
                            let ratingData = movieData.ratings.map(data => data.email)
                                if(ratingData.includes(req.decoded.email)){
                                    res.json({
                                        success: false,
                                        msg: 'user already rated'
                                    })
                                }else{
                                    let updateRating = await dbMovies.findOneAndUpdate({_id: req.params.movieId}, { $addToSet: { ratings: ratingObj}}, { runValidators: true })
                                    res.json({
                                        success: true,
                                        msg: 'Rating added'
                                    })
                                }      
                        }catch(err){
                            res.json({
                                success: false,
                                msg: 'err in updating rating',
                                err: err
                            })
                        }
                    }
                }
            }catch(err){
                res.json({
                    success: false,
                    msg: 'err in fetching movie data',
                    err: err
                })
            }
        }
    }catch(err) {
        res.json({
            success: false,
            msg: 'server err',
            err: err
        })
    }
}