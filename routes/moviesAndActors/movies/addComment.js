const dbLogin = require('../../../models/user/userLogin')
const dbMovies = require('../../../models/movieActor/movies')




module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                suuccess: false,
                msg: 'something went wrong or user not loggedin'
            })
        }else{
            try{
                let movieData = await dbMovies.findById(req.params.movieId)
                if(!movieData || movieData == null){
                    res.json({
                        success: false,
                        msg: 'Movie not Found'
                    })
                }else{
                    if(!req.body.text){
                        res.json({
                            success: false,
                            msg: 'Please add comment'
                        })
                    }else{
                        try{
                            const commentObj = {
                                email: req.decoded.email,
                                name: loginData.name,
                                text: req.body.text
                            }
                            let commentData = movieData.comments.map(data => data.email)
                                if(commentData.includes(req.decoded.email)){
                                    res.json({
                                        success: false,
                                        msg: 'user already commented'
                                    })
                                }else{
                                    let updateComment = await dbMovies.findOneAndUpdate({_id: req.params.movieId}, { $addToSet: { comments: commentObj}}, { runValidators: true })
                                    res.json({
                                        success: true,
                                        msg: 'Comment added'
                                    })
                                } 
                        }catch(err){
                            res.json({
                                success: false,
                                msg: 'err in updating comment',
                                err: err
                            })
                        }
                    }
                }
            }catch(err){
                res.json({
                    success: false,
                    msg: 'err in fetching movie data',
                    err: err
                })
            }
        }
    }catch(err) {
        res.json({
            success: false,
            msg: 'something went wrong',
            err: err
        })
    }
}