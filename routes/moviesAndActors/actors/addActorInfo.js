const dbActors = require('../../../models/movieActor/actors')
const dbLogin = require('../../../models/user/userLogin')


module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                success: false,
                msg: 'something went wrong'
            })
        }else{
            try{
                if(req.decoded.userType == 'admin'){
                    console.log('.......', req.decoded.userType)
                    if(!req.body.name || !req.files || !req.body.age){
                        res.json({
                            success: false,
                            msg: 'please provide all details'
                        })
                    }else{
                        let actorData = await dbActors.findOne({$and: [{name: req.body.name}, { age: req.body.age}]})
                        if(actorData || actorData != null){
                            res.json({
                                success: false,
                                msg: 'actorData already exists'
                            })
                        }else{
                           await new dbActors({
                                    name: req.body.name,
                                    picture: req.files['picture'][0].fieldname + '.' + req.files['picture'][0].mimetype,
                                    age: req.body.age
                                }).save()
                                res.json({
                                    success: true,
                                    msg: 'actorData updated'
                                })    
                        }
                    }
                }else{
                    res.json({
                        success: false,
                        msg: "You are not allowed to add Actor info"
                    })
                }
            } catch(err){
                res.json({
                    success: false,
                    msg: 'err in saving data'
                })
            }
        }
    }catch(err){
        res.json({
            success: false,
            msg: 'server err',
            err: err
        })
    }
}