const dbactors = require('../../../models/movieActor/actors')
const dbMovies = require('../../../models/movieActor/movies')
const dbLogin = require('../../../models/user/userLogin')

module.exports = async(req, res) => {
    try{
        let emailData = await dbLogin.findOne({email: req.decoded.email})
            if(!emailData || emailData == null){
                res.json({
                    success: false,
                    msg: 'something went wrong or user is not loggedin'
                })
        }else{
            try{
                if(req.decoded.userType == 'admin'){
                let actorData = await dbactors.findById(req.params.actorId)
                if(!actorData || actorData == null){
                    res.json({
                        success: false,
                        msg: 'actorData not found, first add actor data'
                    })
                }else{
                    if(!req.body.movieId){
                        res.json({
                            success: false,
                            msg: 'please enter movieId'
                        })
                    }else{
                        let movieData = await dbMovies.findOne({_id: req.body.movieId})
                        if(!movieData || movieData == null){
                            res.json({
                                success: false,
                                msg: 'movieData not found'
                            })
                        }else{
                            if(actorData.movies.includes(req.body.movieId)){
                                res.json({
                                    success: false,
                                    msg: 'movie already exists'
                                })
                            }else{
                                let updateMovie = await dbactors.findOneAndUpdate({_id: req.params.actorId}, { $set: { movies: req.body.movieId} })
                                res.json({
                                    success: true,
                                    msg: 'movie updated'   
                                })
                        }
                    }
                    } 
                }
            }else{
                res.json({
                    success: false,
                    msg: "You are not allowed to add movies"
                })
            }
            }catch(err){
                res.json({
                    success: false,
                    msg: 'err in saving data'
                })
            }
        }
        
    }catch(err){
        res.json({
            success: false,
            msg: 'server err',
            err: err
        })
    }
}