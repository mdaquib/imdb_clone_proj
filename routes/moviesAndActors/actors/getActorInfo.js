const dbActor = require('../../../models/movieActor/actors')
const dbLogin = require('../../../models/user/userLogin')

module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == null){
            res.json({
                success: false,
                msg: 'user not loggedin'
            })
        }else{
            try{
                let actorInfo = await dbActor.findById(req.params.actorId)
                if(!actorInfo || actorInfo == null){
                    res.json({
                        success: false,
                        msg: 'actor not found'
                    })
                }else{
                    res.json({
                        success: true,
                        msg: 'Actor Info',
                        actorinfo: actorInfo
                    })
                }
            }catch(err){
                res.json({
                    success: false,
                    msg: 'err in fetching data',
                    err: err
                })
            }
        }
    }catch(err){
        res.json({
            success: false,
            msg: 'something went wrong',
            err: err
        })
    }
}