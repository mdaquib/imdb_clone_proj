const dbActor = require('../../models/movieActor/actors')
const dbMovies = require('../../models/movieActor/movies')


module.exports = async(req, res) => {
    try{
        if(!req.body.search){
            res.json({
                success: false,
                msg: 'please enter all details'
            })
        }else{
            try{
                var regexSe = new RegExp(req.body.search, 'i')
                let actorData = await dbActor.find({name: regexSe})
                console.log(actorData.length)
                if(actorData.length != 0){
                    res.json({
                        success: true,
                        msg: 'Search result',
                        actor: actorData
                    })
                }else{
                    try{
                        let movieData = await dbMovies.find({name: regexSe})
                        if(movieData.length != 0){
                        let totalRatings = movieData[0].ratings
                                .map(data => data.rating)
                                .reduce((a,b) => a + b)
                            let aggRatings = (totalRatings/movieData[0].ratings.length).toFixed(1)

                            let commentData = movieData[0].comments
                                .map(data => data.text)
                            
                            let mapObj = movieData.map(movie => {
                                return {
                                    name: movie.name,
                                    photos: movie.photos,
                                    description: movie.description,
                                    genre: movie.genre,
                                    cast: movie.cast,
                                    releaseDate: movie.releaseDate, 
                                }
                            })
                            res.json({
                                success: true,
                                msg: 'search result',
                                movie: mapObj,
                                ratings: aggRatings,
                                comments: commentData
                            })
                        }else{
                            res.json({
                                success: false,
                                msg: 'search data not found'
                            })
                        }
                    }catch(err) {
                        res.json({
                            success: false,
                            msg:'err in fetching movie data'
                        })
                    }
                } 
            }catch(err) {
                res.json({
                    success: false,
                    msg:'err in fetching actor data'
                })
            }
        }
    }catch(err) {
        res.json({
            success: false,
            msg: 'something went wrong'
        })
    }
}