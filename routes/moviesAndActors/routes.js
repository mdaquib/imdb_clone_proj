const express = require('express')
const router = express.Router()
const multer = require('multer')
const tokenVerify = require('../../strategies/tokenVerify')

//file upload
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname +'/photos/')
    },
    filename: function (req, file, cb) {
      cb(null, 'img' + Date.now() + '' + file.originalname)
    }
  })
var upload = multer({ storage: storage })



// actor route
var profileUpload = upload.fields([{ name: 'picture', maxCount: 1 }, { name: 'gallery', maxCount: 6 }])
router.post('/addActorInfo', tokenVerify, profileUpload, require('./actors/addActorInfo'))
router.post('/addMovies/:actorId', tokenVerify, require('./actors/addmovies'))
//get
router.get('/getActorInfo/:actorId', tokenVerify, require('./actors/getActorInfo'))


// movies route
router.post('/addActor/:movieId', tokenVerify, require('./movies/addActors'))
router.post('/addComment/:movieId', tokenVerify, require('./movies/addComment'))

var photoUpload = upload.fields([{ name: 'photo', maxCount: 1 }, { name: 'gallery', maxCount: 6 }])
router.post('/addMovieData', tokenVerify, photoUpload, require('./movies/addmovieData'))
router.post('/addRating/:movieId', tokenVerify, require('./movies/addRating'))

router.post('/search', require('./searchMovieActor'))
router.post('/getMovieGenre', tokenVerify, require('./movies/getMovieGenre'))
//public, get all movies acc to latest added.
router.get('/getAllmovies', require('./movies/getAllMovies'))


module.exports = router