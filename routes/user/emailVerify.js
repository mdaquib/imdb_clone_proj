const dbRegister = require('../../models/registration/register')
const dbLogin = require('../../models/user/userLogin')
const dbAdminLogin = require('../../models/admin/adminLogin')
const dbProfile = require('../../models/registration/profile')
const nodeMailer = require('../nodeMailer/nodeMailer')
const bcrypt = require('bcrypt')
const saltRounds = 8


module.exports = (req, res) => {
    if(!req.body.emailOtp){
        res.json({
            success: false,
            msg: 'please enter 6 digit otp'
        })
    }else{
        dbRegister.findOne({email: req.decoded.email})
        .then(registeredData => {
            console.log('///////', registeredData.userType)
            if(!registeredData || registeredData == null){
                res.json({
                    success: false,
                    msg: 'Please register first'
                })
            }else if(registeredData.emailVerify.otp == req.body.emailOtp){
                var login = new dbLogin({
                    name: registeredData.name,
                    phone: registeredData.phone,
                    email: registeredData.email,
                    age: registeredData.age,
                    password: registeredData.password,
                    userType: registeredData.userType,
                    status: 1
                })
                bcrypt.hash(registeredData.password, saltRounds)
                .then((hash) => {
                    login.password = hash
                    login.save()
                    .then(loginData => {
                        new dbProfile({
                            name: registeredData.name,
                            phone: registeredData.phone,
                            email: registeredData.email,
                            age: registeredData.age,
                            userType: registeredData.userType,
                            status: 2
                        })
                        .save()
                        .then(profileData => {
                            dbRegister.findOneAndUpdate({email: req.decoded.email}, { $set: { status: 1, 'emailVerify.verified': true, 'emailVerify.verifiedAt': new Date() } })
                            .then(data => {
                                if(data.userType == 'admin'){
                                    new dbAdminLogin({
                                        email: data.email,
                                        name: data.name,
                                        password: registeredData.password
                                    })
                                    .save()
                                    .then(adminData => {
                                        res.json({
                                            success: true,
                                            msg: 'admin successfully registered'
                                        })
                                    })
                                    .catch(err => {
                                        res.json({
                                            success: false,
                                            msg: 'err in saving admin data'
                                        })
                                    })
                                }else{
                                     let subject = 'welcome to IMDb'
                                        nodeMailer.sendMails(data.email, subject)
                                        .then(mailSent => {
                                            res.json({
                                                success: true,
                                                msg: 'user Successfully registered'
                                            }) 
                                        })
                                        .catch(err => {
                                            res.json({
                                                success: false,
                                                msg: 'err in sending mail',
                                                err: err
                                            })
                                        })   
                                }                 
                            })
                            .catch(err => {
                                res.json({
                                    success: false,
                                    msg: 'err in updating Data',
                                    err: err
                                })
                            })
                        })
                        .catch(err => {
                            res.json({
                                success: false,
                                msg: 'err in saving profile data',
                                err: err
                            })
                        })
                    })
                        .catch(err => {
                            res.json({
                                success: false,
                                msg: 'err in saving login Data',
                                err: err
                            })
                        })
                })
                .catch(err => {
                    res.json({
                        success: false,
                        msg: 'err in hashing password',
                        err: err
                    })
                })
            }else{
                res.json({
                    success: false,
                    msg: 'Incorrect OTP'
                })
            }
        })
        .catch(err => {
            res.json({
                success: false,
                msg: 'server error',
                err: err
            })
        })
    }
}