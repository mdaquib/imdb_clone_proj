const dbProfile = require('../../models/registration/profile')
const dbLogin = require('../../models/user/userLogin')


module.exports = async(req, res) => {
    try{
        let loginData = await dbLogin.findOne({email: req.decoded.email})
        if(!loginData || loginData == 0){
            console.log(loginData)
            res.json({
                success: false,
                msg: 'User not logged in'
            })
        }else{
            try{
                let profileData = await dbProfile.find({email: req.decoded.email})
                res.json({
                    success: false,
                    msg: 'Profile Data',
                    profile: profileData
                })
            } catch(err) {
                res.json({
                    success: false,
                    mag: 'err in fetching data',
                    err: err
                })
            }
            
        }
    } catch(err){
        res.json({
            success: false,
            msg: 'server err',
            err: err
        })
    }
    

}