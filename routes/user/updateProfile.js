const dbProfile  = require('../../models/registration/profile')
const dbLogin = require('../../models/user/userLogin')

module.exports = (req, res) => {
    dbLogin.findOne({email: req.decoded.email})
    .then(loginData => {
        if(!loginData || loginData == null){
            res.json({
                success: false,
                msg: 'User details does not exists'
            })
        }else{
            dbProfile.findOne({email: req.decoded.email})
            .then(profileData => {
                if(!profileData || profileData == null){
                    res.json({
                        success: false,
                        msg: "You haven't registered or completed the registration process yet."
                    })
                }else{
                    console.log(req.files)
                    const object = { 
                                    profilePic: req.files['profilePicture'][0].fieldname + '.' + req.files['profilePicture'][0].mimetype,
                                    status: 3
                                }
                        
                    dbProfile.findOneAndUpdate({email: req.decoded.email}, {$set: object}, { runValidators: true })
                    .then(profileUpdated => {
                        if(profileUpdated){
                            dbProfile.findOne({email: req.decoded.email})
                            .then(profileData => {
                                res.json({
                                    success: true,
                                    msg: 'Profile Updated',
                                    newData: profileData
                                })
                            })
                            .catch(err => {
                                res.json({
                                    success: false,
                                    msg: 'something went wrong',
                                    err: err
                                })
                            })
                        }
                    })
                    .catch(err => {
                        res.json({
                            success: false,
                            msg: 'err in updating profile data',
                            err: err
                        })
                    })
                }
            })
            .catch(err => {
                res.json({
                    success: false,
                    msg:'err in fetching profile data',
                    err: err
                })
            })
        }
    })
    .catch(err => {
        res.json({
            success: false,
            msg: 'loginData not Found, Please try again',
            err: err
        })
    })
}