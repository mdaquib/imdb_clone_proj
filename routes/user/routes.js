const express = require('express')
const multer = require('multer')
const router = express.Router()
const tokenVerify = require('../../strategies/tokenVerify')
const registerValidate = require('./validation/registerValidate')
const loginValidate = require('./validation/loginValidate')
const changePassVali = require('./validation/changePassVali')

//multer, file upload
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname +'/pictures/')
    },
    filename: function (req, file, cb) {
      cb(null, 'img' + Date.now() + '' + file.originalname)
    }
  })
var upload = multer({ storage: storage })


//register login logout
router.post('/register', registerValidate, require('./register'))
router.post('/emailVerify', tokenVerify, require('./emailVerify'))
router.post('/login', loginValidate, require('./loginLogout').login)
router.post('/logout', tokenVerify, require('./loginLogout').logout)
router.post('/resend', tokenVerify, require('./resendOtp'))

// profile
var cpUpload = upload.fields([{ name: 'profilePicture', maxCount: 1 }, { name: 'gallery', maxCount: 8 }])
const profile = require('./updateProfile')
router.put('/profileUpdate', tokenVerify, cpUpload, profile)
router.get('/profileData', tokenVerify, require('./profileData'))

// Change Forgot Password
router.post('/changePass', changePassVali, tokenVerify, require('./changeForgot').changePass)
router.post('/forgotPass', require('./changeForgot').forgotPass)


module.exports = router