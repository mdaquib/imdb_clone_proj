const dbRegister = require('../../models/registration/register')
const mailer = require('../nodeMailer/nodeMailer')
const jwt = require('jsonwebtoken')

// generate OTP
var generateOTP = () => {
    return Math.floor(100000 + Math.random() * 900000);
  };

module.exports = (req, res) => {
    if(!req.body.email || !req.body.password || !req.body.phone || !req.body.age || !req.body.name || !req.body.userType){
        res.json({
            success: false,
            msg: 'Please provide all Details'
        })
    }else{
        dbRegister.findOne({$or: [{ email: req.body.email }, { phone: req.body.phone }]})
            .then(data => {
            if (data) {
                    res.json({
                    success: false,
                    msg: "User Email or Phone already registered"
                    });
                } else {
                    new dbRegister({
                        name: req.body.name,
                        email: req.body.email,
                        age: req.body.age,
                        phone: req.body.phone,
                        password: req.body.password,
                        userType: req.body.userType,
                        status: 0,
                        emailVerify: {
                          otp: generateOTP(),
                          verified: false
                        }
                    })
            .save()
            .then(userData => {
              let msg = `Your otp for email verification is :- ${userData.emailVerify.otp}`;
              console.log(msg)
              if (userData) {
                let token = jwt.sign({email: req.body.email, phone: req.body.phone, age: req.body.age},req.app.get("secretKey"),{ expiresIn: "1h" });
                    dbRegister.findOneAndUpdate({ email: req.body.email },{ $set: { token: token } })
                    .then(updated => {
                        mailer.sendMails(req.body.email, msg)
                        .then(mailSent => {
                          res.json({
                            success: true,
                            msg: "please verify email",
                            token: token
                            });
                        })
                        .catch(err => {
                          res.json({
                            success: false,
                            msg: 'err in sending mail',
                            err: err
                          })
                        });
                        })
                    .catch(err => {
                      res.json({
                        success: false,
                        msg: 'err in updating Data',
                        err: err
                      })
                    });
              } else {
                res.json({
                  success: false,
                  msg: "something went wrong"
                });
              }
            })
            .catch(err => {
              res.json({
                success: false,
                msg: 'err in saving data',
                err: err
              })
            })
      }
    })
    .catch(err => {
      res.json({
        success: false,
        msg: 'db error',
        err: err
      })
    })
    }
}