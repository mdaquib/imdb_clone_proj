const helmet = require('helmet');
const compression = require('compression')


module.exports = async function(app) {
    app.use(helmet())
    app.use(compression())
}